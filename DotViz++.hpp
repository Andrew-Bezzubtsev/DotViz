#ifndef DOTVIZ_PLUS_PLUS_HPP
#define DOTVIZ_PLUS_PLUS_HPP

#include <string>

namespace DotViz {
	class DVNodeStyle {
		::std::string __shape__;
		::std::string __color__;
		::std::string __style__;
		::std::string __fill_color__;
		::std::string __font_color__;

	public:
		DVNodeStyle(::std::string shape = "box",
		            ::std::string color = "#FFDEAD",
		            ::std::string style = "rounded, solid, filled",
		            ::std::string fill_color = "#FFDEAD",
		            ::std::string font_color = "#FF0000");
		~DVNodeStyle();

		DVNodeStyle &shape(::std::string val);
		::std::string shape() const;

		DVNodeStyle &color(::std::string val);
		::std::string color() const;

		DVNodeStyle &style(::std::string val);
		::std::string style() const;
		
		DVNodeStyle &fillColor(::std::string val);
		::std::string fillColor() const;

		DVNodeStyle &fontColor(::std::string val);
		::std::string fontColor() const;

		DVNodeStyle &operator=(const DVNodeStyle &src);
		bool operator==(const DVNodeStyle &val) const;
		bool operator!=(const DVNodeStyle &val) const;
	};

	class DVLinkStyle {
		::std::string __color__;
		::std::string __style__;
	
	public:
		DVLinkStyle(::std::string color = "#9ACD32",
		            ::std::string style = "solid");
		~DVLinkStyle();

		DVLinkStyle &color(::std::string val);
		::std::string color() const;

		DVLinkStyle &style(::std::string val);
		::std::string style() const;

		DVLinkStyle &operator=(const DVLinkStyle &src);
		bool operator==(const DVLinkStyle &val) const;
		bool operator!=(const DVLinkStyle &val) const;
	};

	class DVGraph {
		DVNodeStyle __ns__;
		DVLinkStyle __ls__;
		FILE *__out__;
		bool __directed__;
	public:
		DVGraph();
		~DVGraph();

		void begin(::std::string path, ::std::string name, bool directed = true);

		void node(long long unsigned id, ::std::string label);
		DVNodeStyle &nodeStyle();
		DVNodeStyle &nodeStyle(const DVNodeStyle &src);

		void link(long long unsigned id0, long long unsigned id1);
		DVLinkStyle &linkStyle();
		DVLinkStyle &linkStyle(const DVLinkStyle &src);

		void end();
	};

	void dvRender(::std::string in, ::std::string out);
}

#endif // DOTVIZ_PLUS_PLUS_HPP
