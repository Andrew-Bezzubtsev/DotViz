#include <string>
#include <sstream>
#include <cstdio>
#include <cstdlib>

#include <errno.h>
#include <string.h>

#ifndef _MSC_VER
#include <stdint.h>
#else
#define __PRETTY_FUNCTION__ __FUNCTION__
#endif

#ifdef _WIN32
#define popen _popen
#define pclose _pclose
#define U64pf "%I64u"
#else
#define U64pf "%llu"
#endif

#include "DotViz++.hpp"

#define PANIC(mode, type, fmt, ...) \
{ \
	std::fprintf(stderr, fmt \
	             "Error comes from %d line, %s function from %s file", ##__VA_ARGS__, \
	             __LINE__, __FUNCTION__, __FILE__); \
	if (mode) \
		return type(); \
}

#define CHECK_PROCEEDING(type) \
if (!__out__) \
	PANIC(true, type, "DotViz's not proceeding now!\n");

#define CHECK_NOT_PROCEEDING(type) \
if (__out__) \
	PANIC(true, type, "DotViz's proceeding now\n");

#define CHECK_NODE_STYLE(type) \
if (!nodeStyle().shape().length() || \
    !nodeStyle().color().length() || \
    !nodeStyle().style().length() || \
    !nodeStyle().fillColor().length() || \
    !nodeStyle().fontColor().length()) \
	PANIC(false, type, "DotViz's node_style hasn't got some member");

#define CHECK_LINK_STYLE(type) \
if (!linkStyle().color().length() || \
    !linkStyle().style().length()) \
	PANIC(false, type, "DotViz's link_style hasn't got some member");

namespace DotViz {
	DVNodeStyle::DVNodeStyle(::std::string shape /*= "box"*/,
	                         ::std::string color /*= "#FFDEAD"*/,
	                         ::std::string style /*= "rounded, solid, filled"*/,
	                         ::std::string fill_color /*= "#FFDEAD"*/,
	                         ::std::string font_color /*= "#FF0000"*/):
		__shape__(shape),
		__color__(color),
		__style__(style),
		__fill_color__(fill_color),
		__font_color__(font_color)
	{ }

	DVNodeStyle::~DVNodeStyle()
	{ }

	DVNodeStyle &DVNodeStyle::shape(::std::string val)
	{
		__shape__ = val;
		return *this;
	}

	DVNodeStyle &DVNodeStyle::color(::std::string val)
	{
		__color__ = val;
		return *this;
	}

	DVNodeStyle &DVNodeStyle::style(::std::string val)
	{
		__style__ = val;
		return *this;
	}

	DVNodeStyle &DVNodeStyle::fillColor(::std::string val)
	{
		__fill_color__ = val;
		return *this;
	}

	DVNodeStyle &DVNodeStyle::fontColor(::std::string val)
	{
		__font_color__ = val;
		return *this;
	}

	::std::string DVNodeStyle::shape() const
	{
		return __shape__;
	}

	::std::string DVNodeStyle::color() const
	{
		return __color__;
	}

	::std::string DVNodeStyle::style() const
	{
		return __style__;
	}

	::std::string DVNodeStyle::fillColor() const
	{
		return __fill_color__;
	}

	::std::string DVNodeStyle::fontColor() const
	{
		return __font_color__;
	}

	DVNodeStyle &DVNodeStyle::operator=(const DVNodeStyle &src)
	{
		__shape__ = src.__shape__;
		__color__ = src.__color__;
		__style__ = src.__style__;
		__fill_color__ = src.__fill_color__;
		__font_color__ = src.__font_color__;

		return *this;
	}

	bool DVNodeStyle::operator==(const DVNodeStyle &val) const
	{
		return (__shape__ == val.__shape__ &&
		        __color__ == val.__color__ &&
		        __style__ == val.__style__ &&
		        __fill_color__ == val.__fill_color__ &&
		        __font_color__ == val.__font_color__);
	}

	bool DVNodeStyle::operator!=(const DVNodeStyle &val) const
	{
		return !(val == *this);
	}

	DVLinkStyle::DVLinkStyle(::std::string color /*= "#9ACD32"*/,
	                         ::std::string style /*= "solid"*/):
		__color__(color),
		__style__(style)
	{ }

	DVLinkStyle::~DVLinkStyle()
	{ }

	DVLinkStyle &DVLinkStyle::color(::std::string val)
	{
		__color__ = val;
		return *this;
	}

	DVLinkStyle &DVLinkStyle::style(::std::string val)
	{
		__style__ = val;
		return *this;
	}

	::std::string DVLinkStyle::color() const
	{
		return __color__;
	}

	::std::string DVLinkStyle::style() const
	{
		return __style__;
	}

	DVLinkStyle &DVLinkStyle::operator=(const DVLinkStyle &src)
	{
		__color__ = src.__color__;
		__style__ = src.__style__;

		return *this;
	}

	bool DVLinkStyle::operator==(const DVLinkStyle &src) const
	{
		return (__color__ == src.__color__ &&
		        __style__ == src.__style__);
	}

	bool DVLinkStyle::operator!=(const DVLinkStyle &src) const
	{
		return !(src == *this);
	}

	DVGraph::DVGraph():
	    __ns__(),
	    __ls__(),
	    __out__(0),
	    __directed__(false)
	{ }

	DVGraph::~DVGraph()
	{ }

	void DVGraph::begin(::std::string path, ::std::string name, bool directed /* = true*/)
	{
		CHECK_NOT_PROCEEDING(void)
		__out__ = ::std::fopen(path.c_str(), "w");
		__directed__ = directed;
		CHECK_PROCEEDING(void)

		::std::fprintf(__out__, "# By DotViz++\n%sgraph %s {\n", ((directed)?("di"):("")), name.c_str());
	}

	void DVGraph::node(long long unsigned id, ::std::string label)
	{
		CHECK_PROCEEDING(void)
		CHECK_NODE_STYLE(void)

		::std::fprintf(__out__,
			"\tNode_" U64pf "["
			"shape=\"%s\", "
			"color=\"%s\", "
			"style=\"%s\", "
			"fillcolor=\"%s\", "
			"fontcolor=\"%s\", "
			"weight=\"1\", "
			"label=\"%s\"];\n",
			id,
            nodeStyle().shape().c_str(),
			nodeStyle().color().c_str(),
			nodeStyle().style().c_str(),
			nodeStyle().fillColor().c_str(),
			nodeStyle().fontColor().c_str(),
			label.c_str());

		CHECK_NODE_STYLE(void)
		CHECK_PROCEEDING(void)
	}

	DVNodeStyle &DVGraph::nodeStyle()
	{
		return __ns__;
	}

	DVNodeStyle &DVGraph::nodeStyle(const DVNodeStyle &src)
	{
		__ns__ = src;
		return nodeStyle();
	}

	void DVGraph::link(long long unsigned id0, long long unsigned id1)
	{
		CHECK_PROCEEDING(void)
		CHECK_LINK_STYLE(void)

		::std::fprintf(__out__,
			"\tNode_" U64pf "%s Node_" U64pf "["
			"style=\"%s\", "
			"color=\"%s\"];\n",
			id0, ((__directed__)?("->"):("--")), id1,
			linkStyle().style().c_str(),
			linkStyle().color().c_str());

		CHECK_LINK_STYLE(void)
		CHECK_PROCEEDING(void)
	}

	DVLinkStyle &DVGraph::linkStyle()
	{
		return __ls__;
	}

	DVLinkStyle &DVGraph::linkStyle(const DVLinkStyle &src)
	{
		__ls__ = src;
		return linkStyle();
	}

	void DVGraph::end()
	{
		CHECK_PROCEEDING(void)

		::std::fprintf(__out__, "}\n# By DotViz++\n");
		::std::fclose(__out__);
	}

	void dvRender(::std::string in, ::std::string out)
	{
		auto get_ext_from_path = [](::std::string path) -> ::std::string {
			size_t ids[2] = {
				path.rfind("\\"),
				path.rfind("/")
			};

			size_t id;

			if (ids[0] == ::std::string::npos &&
			    ids[1] == ::std::string::npos)
				id = 0;
			else if (ids[0] == ::std::string::npos)
				id = ids[1] + 1;
			else
				id = ids[0] + 1;
	
			path = path.substr(id);
		
			size_t dot_p = path.rfind(".");
			if (dot_p != ::std::string::npos)
				return path.substr(dot_p + 1);

			return "";
		};

		auto syscmd = [](::std::string cmd, ::std::string &res) -> void {
			FILE *file;
			if (!(file = popen(cmd.c_str(), "r"))) {
				PANIC(0, void, "Error!\nCan't exec '%s': %s!",
				cmd.c_str(), strerror(errno));
			}

			char buf[4096] = "";
			if (fgets(buf, sizeof(buf) - 1, file))
			res = buf;

			pclose(file);
		};

		::std::string type = get_ext_from_path(out);
		type = ((type.length())?(type):("pdf"));

		::std::stringstream val;
		val << "dot" << " -T" << type << " -o " << out << " " << in;

		::std::string res = "";
		syscmd(val.str(), res);
	}
}

#undef PANIC
#undef CHECK_PROCEEDING
#undef CHECK_NOT_PROCEEDING
#undef CHECK_NODE_STYLE
#undef CHECK_LINK_STYLE
