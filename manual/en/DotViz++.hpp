//{===========================================================================================================
/**
 * @file DotViz++.hpp
 *
 * @par DotViz (Dot Vizualiser) 
 * It is a small C++ library, providing dumping graphs into
 * images. Every graph, being dumped by DotViz, is presented as an object. So,
 * because of object-orintied approach, multiple graph dump is possible. 
 * I. e this approach provides ability of dumping several graphs at the 
 * same time.
 *
 * @par Where DotViz can be useful?
 * Do you remember any non-linear data structure? E. g binary search tree...
 * And, do you represent the text dump of it?
 * It should be something alike:
 * 
 * @code
 * tree [0x8200402F] {
 *     children [0x8200402F + 0xFF] {
 *         tree [0x8200402F + 0xFF] {
 *			...
 *         }
 *     }
 * }
 * @endcode
 *
 * So, I think that graphical dump done by DotViz is better.
 * 
 * @par Why graphical dump is better?
 * Representing the tree in your head will take a lot of time...
 * But, if dumping it as image, you don't need representing it. It will be
 * presented at the time and you will see everthing: all data and 
 * connections between the objects of your data structure.
 *
 * $Copyright: (C) 2015-2016 by Andrew Bezzubtsev $
 * $Date: Thu, 23 Jun 2016 $
 * $Rev: 18 $
 */
//============================================================================================================
/**
 * @defgroup style Graph drawing style
 * @defgroup stuff Main stuff doing dump & rendering
 */
//}===========================================================================================================

#ifndef DOTVIZ_PLUS_PLUS_HPP
#define DOTVIZ_PLUS_PLUS_HPP

#include <string>

namespace DotViz {
	//{===========================================================================================================
	/**
	 * @ingroup style
	 * DotViz node rendering style. The class contains some properties,
	 * requested due drawing/rendering node. In partucular, they are:
	 * 
	 * <ul>
	 *   <li>shape of node</li>
	 *   <li>style of drawing cirquit</li>
	 *   <li>color of circuit</li>
	 *   <li>fill color</li>
	 *   <li>font color</li>
	 * </ul>
	 *
	 * @code
	 * void changeStyle(DotViz::DVGraph &style)
	 * {
	 *     DotViz::DVNodeStyle style("egg", "#ADDEFF");
	 *     graph.nodeStyle(style);
	 * }
	 * @endcode
	 *
	 * @see DVLinkStyle, DVGraph.
	 */
	//}===========================================================================================================
	class DVNodeStyle {
		::std::string __shape__;
		::std::string __color__;
		::std::string __style__;
		::std::string __fill_color__;
		::std::string __font_color__;
	public:
		//{================================================================================================================
		/**
		 * Default DVNodeStyle constructor.
		 *
		 * @arg shape shape of the node
		 * @arg color color of the node
		 * @arg style style of node's drawing
		 * @arg fill_color fill color of the node
		 * @arg font_color font color of the node
		 *
		 * It creates and initializes a DVNodeStyle object with
		 * given/default values
		 *
		 * @code
		 * DotViz::DVGraph dump;
		 * ...
		 * DotViz::DVNodeStyle new_style("egg", "#FFDEAD", "rounded, solid");
		 * dump.nodeStyle(new_style);
		 * ...
		 * @endcode
		 *
		 * @see ~DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================
		DVNodeStyle(::std::string shape = "box", 
		            ::std::string color = "#FFDEAD",
		            ::std::string style = "rounded, solid, filled", 
		            ::std::string fill_color = "#FFDEAD",
		            ::std::string font_color = "#FF0000");
		//{================================================================================================================
		/**
		 * Default DVNodeStyle destructor.
		 *
		 * This function destroys DVNodeStyle object.
		 */
		//}================================================================================================================
		~DVNodeStyle();

		//{================================================================================================================
		/**
		 * Modify shape of being rendered nodes.
		 * 
		 * @arg val new shape of nodes
		 *
		 * This function substitutes the previous value of nodes' shape.
		 *
		 * Every node, drawn before the shape substitution, will save its
		 * shape.
		 *
		 * @code
		 * void makeEllipse(DotViz::DVNodeStyle &ns)
		 * {
		 *     node_style.shape("ellipse");
		 * }
		 * @endcode
		 *
		 * @return Reference to node style
		 *
		 * @see DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================
		inline DVNodeStyle &shape(::std::string val);

		//{================================================================================================================
		/**
		 * Get shape of being rendered nodes.
		 *
		 * The function gets the current value of nodes' shape.
		 *
		 * @code
		 * void printData(DotViz::DVNodeStyle ns)
		 * {
		 *     std::cout << "Shape: " << ns.shape() << ";\n"
		 *               << "Color: " << ns.color() << ";\n"
		 *               << "Style: " << ns.style() << ";\n"
		 *               << "Fill color: " << ns.fillColor() << ";\n"
		 *               << "Font color: " << ns.fontColor() << ";\n";
		 * }
		 * @endcode
		 *
		 * @return Current nodes' shape as string
		 *
		 * @see DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================
		inline ::std::string shape() const;

		//{================================================================================================================
		/**
		 * Modify circuit color of being rendered nodes.
		 * 
		 * @arg val new circuit color of nodes
		 *
		 * This function substitutes the previous value of nodes'
		 * circuit color.
		 *
		 * Every node, drawn before the circuit color substitution, will save its
		 * circuit color.
		 *
		 * @code
		 * void makeRed(DotViz::DVNodeStyle &ns)
		 * {
		 *     node_style.color("#FF0000");
		 * }
		 * @endcode
		 *
		 * @return Reference to node style
		 *
		 * @see DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================
		inline DVNodeStyle &color(::std::string val);

		//{================================================================================================================
		/**
		 * Get color of being rendered nodes.
		 *
		 * The function gets the current value of nodes' color.
		 *
		 * @code
		 * void printData(DotViz::DVNodeStyle ns)
		 * {
		 *     std::cout << "Shape: " << ns.shape() << ";\n"
		 *               << "Color: " << ns.color() << ";\n"
		 *               << "Style: " << ns.style() << ";\n"
		 *               << "Fill color: " << ns.fillColor() << ";\n"
		 *               << "Font color: " << ns.fontColor() << ";\n";
		 * }
		 * @endcode
		 *
		 * @return color of nodes' drawing style as string
		 *
		 * @see DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================
		inline ::std::string color() const;
		
		//{================================================================================================================
		/**
		 * Modify style of being rendered nodes.
		 * 
		 * @arg val new style of nodes
		 *
		 * This function substitutes the previous value of nodes' style.
		 *
		 * Every node, drawn before the style substitution will save its
		 * style.
		 *
		 * @code
		 * void makeDotted(DVNodeStyle &ns)
		 * {
		 *     node_style.style("dotted");
		 * }
		 * @endcode
		 *
		 * @return Reference to node style
		 *
		 * @see DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================
		inline DVNodeStyle &style(::std::string val);

		//{================================================================================================================
		/**
		 * Get style of being rendered nodes.
		 *
		 * The function gets the current value of nodes' style.
		 *
		 * @code
		 * void printData(DotViz::DVNodeStyle ns)
		 * {
		 *     std::cout << "Shape: " << ns.shape() << ";\n"
		 *               << "Color: " << ns.color() << ";\n"
		 *               << "Style: " << ns.style() << ";\n"
		 *               << "Fill color: " << ns.fillColor() << ";\n"
		 *               << "Font color: " << ns.fontColor() << ";\n";
		 * }
		 * @endcode
		 *
		 * @return Current circuit drawing color as string
		 *
		 * @see DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================
		inline ::std::string style() const;

		//{================================================================================================================
		/**
		 * Modify fill color of being rendered nodes.
		 * 
		 * @arg val new fill color of nodes
		 *
		 * This function substitutes the previous value of nodes' fill
		 * color.
		 *
		 * Every node, drawn before the fill color substitution will save its
		 * fill color.
		 *
		 * @code
		 * void makeRedFill(DotViz::DVNodeStyle &ns)
		 * {
		 *     node_style.fillColor("red");
		 * }
		 * @endcode
		 *
		 * @return Reference to node style
		 *
		 * @see DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================		
		inline DVNodeStyle &fillColor(::std::string val);

		//{================================================================================================================
		/**
		 * Get fill color of being rendered nodes.
		 *
		 * The function gets the current value of nodes' fill color.
		 *
		 * @code
		 * void printData(DotViz::DVNodeStyle ns)
		 * {
		 *     std::cout << "Shape: " << ns.shape() << ";\n"
		 *               << "Color: " << ns.color() << ";\n"
		 *               << "Style: " << ns.style() << ";\n"
		 *               << "Fill color: " << ns.fillColor() << ";\n"
		 *               << "Font color: " << ns.fontColor() << ";\n";
		 * }
		 * @endcode
		 *
		 * @return Current fill color of nodes' as string
		 *
		 * @see DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================
		inline ::std::string fillColor() const;

		//{================================================================================================================
		/**
		 * Modify font color of being rendered nodes.
		 * 
		 * @arg val new font color of nodes
		 *
		 * This function substitutes the previous value of nodes' font
		 * color.
		 *
		 * Every node, drawn before the font color substitution will save its
		 * font color.
		 *
		 * @code
		 * void makeGreenFont(DotViz::DVNodeStyle &ns)
		 * {
		 *     node_style.fontColor("green");
		 * }
		 * @endcode
		 *
		 * @return Reference to node style
		 *
		 * @see DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================		
		inline DVNodeStyle &fontColor(::std::string val);

		//{================================================================================================================
		/**
		 * Get font color of being rendered nodes.
		 *
		 * The function gets the current value of nodes' font color.
		 *
		 * @code
		 * void printData(DotViz::DVNodeStyle ns)
		 * {
		 *     std::cout << "Shape: " << ns.shape() << ";\n"
		 *               << "Color: " << ns.color() << ";\n"
		 *               << "Style: " << ns.style() << ";\n"
		 *               << "Fill color: " << ns.fillColor() << ";\n"
		 *               << "Font color: " << ns.fontColor() << ";\n";
		 * }
		 * @endcode
		 *
		 * @return Current nodes' font color as string
		 *
		 * @see DVNodeStyle(), shape(), color(), style(), fillColor(),
		 * fontColor().
		 */
		//}================================================================================================================
		inline ::std::string fontColor() const;

		inline DVNodeStyle &operator=(const DVNodeStyle &src);
		inline bool operator==(const DVNodeStyle &val) const;
		inline bool operator!=(const DVNodeStyle &val) const;
	};

	//{================================================================================================================
	/**
	 * @ingroup style
	 *
	 * DotViz link rendering style.
	 *
	 * This class contains link drawing data. They are:
	 * <ul>
	 *   <li>color of the link</li>
	 *   <li>drawing style of the link</li>
	 * </ul>
	 *
	 * @code
	 * DotViz::DVGraph graph;
	 * ...
	 * DotViz::DVLinkStyle link_style("0xFF0000");
	 * graph.linkStyle(link_style);
	 * @endcode
	 *
	 * @see DVNodeStyle, DVGraph.
	 */
	//}================================================================================================================
	class DVLinkStyle {
		::std::string __color__;
		::std::string __style__;
	public:
		//{================================================================================================================
		/**
		 * Default DVLinkStyle constructor.
		 *
		 * @arg color color of links
		 * @arg style drawing style of links
		 *
		 * This function createds and initializes DVLinkStyle object
		 * with given/default value.
		 *
		 *
		 * @code
		 * DotViz::DVGraph dump;
		 * ...
		 * DotViz::DVNodeStyle new_style("#FFF000");
		 * dump.linkStyle(new_style);
		 * @endcode
		 *
		 * @see ~DVLinkStyle(), color(), style().
		 */
		DVLinkStyle(::std::string color = "#9ACD32",
		            ::std::string style = "solid");
		//{================================================================================================================
		/**
		 * Default DVNodeStyle destructor
		 *
		 * This function destroys DVLinkStyle object.
		 */
		//}================================================================================================================
		~DVLinkStyle();

		//{================================================================================================================
		/**
		 * Modify color of being rendered links.
		 *
		 * @arg val new color of links.
		 *
		 * This function substitutes the previous value of links'
		 * drawing color.
		 *
		 * Every link, drawn before the color substitution, will save
		 * its color.
		 *
		 * @code
		 * DotViz::DVGraph dump;
		 * ...
		 * DotViz::DVLinkStyle style;
		 * style.color("red");
		 * dump.linkStyle(style);
		 * @endcode
		 *
		 * @return Reference to graph's link style
		 *
		 * @see DVLinkStyle() ~DVLinkStyle(), color(), style().
		 */
		//}================================================================================================================
		inline DVLinkStyle &color(::std::string val);
			
		//{================================================================================================================
		/**
		 * Get color of being rendered links.
		 *
		 * This function gets current value of links' color.
		 *
		 * @code
		 * void printData(DotViz::DVLinkStyle style)
		 * {
		 *     std::cout << "Color: " << style.color() << ";\n"
		 *               << "Style: " << style.style() << ";\n";
		 * }
		 * @endcode
		 *
		 * @return Color of links being drawn as string.
		 *
		 * @see DVLinkStyle(), ~DVLinkStyle(), color(), style().
		 */
		//}================================================================================================================
		inline ::std::string color() const;

		//{================================================================================================================
		/**
		 * Modify style of being rendered links.
		 *
		 * @arg val new style of links.
		 *
		 * This function substitutes the previous value of links'
		 * drawing style.
		 *
		 * Every link, drawn before the style substitution, will save
		 * its style.
		 *
		 * @code
		 * DotViz::DVGraph dump;
		 * ...
		 * DotViz::DVLinkStyle style;
		 * style.style("dotted");
		 * dump.linkStyle(style);
		 * @endcode
		 *
		 * @return Reference to links' drawing style.
		 *
		 * @see DVLinkStyle() ~DVLinkStyle(), color(), style().
		 */
		//}================================================================================================================
		inline DVLinkStyle &style(::std::string val);

		//{================================================================================================================
		/**
		 * Get style of being rendered links.
		 *
		 * This function gets current value of links' style.
		 *
		 * @code
		 * void printData(DotViz::DVLinkStyle style)
		 * {
		 *     std::cout << "Color: " << style.color() << ";\n"
		 *               << "Style: " << style.style() << ";\n";
		 * }
		 * @endcode
		 *
		 * @return Current links drawing style as string
		 *
		 * @see DVLinkStyle(), ~DVLinkStyle(), color(), style().
		 */
		//}================================================================================================================
		inline ::std::string style() const;

		inline DVLinkStyle &operator=(const DVLinkStyle &src);
		inline bool operator==(const DVLinkStyle &val) const;
		inline bool operator!=(const DVLinkStyle &val) const;
	};

	//{================================================================================================================
	/**
	 * @ingroup stuff
	 * DotViz graph description class.
	 * This class contains functions, providing description & dumping of
	 * graphs. In particular, they are about node/link creation and style
	 * substitution.
	 *
	 * @code
	 * DotViz::DVGraph graph;
	 * graph.begin("out.gv", "test");
	 *
	 * graph.node(0, "Distribution");
	 * graph.node(1, "Hell");
	 * graph.node(2, "Heaven");
	 *
	 * graph.link(0, 1);
	 * graph.link(0, 2);
	 * @endcode
	 *
	 * @see DVNodeStyle, DVLinkStyle.
	 */
	//}================================================================================================================	
	class DVGraph {
		DVNodeStyle __ns__;
		DVLinkStyle __ls__;
		FILE *__out__;
		bool __directed__;
	public:
		//{================================================================================================================
		/**
		 * DotViz Graph class default constructor.
		 *
		 * This function create DVGraph object.
		 */
		//}================================================================================================================	
		DVGraph();

		//{================================================================================================================
		/**
		 * DotViz Graph class default destructor.
		 *
		 * This function destroys DVGraph object.
		 */
		//}================================================================================================================	
		~DVGraph();

		//{================================================================================================================
		/**
		 * Begin graph description.
		 *
		 * @arg path file to be graph dumped in
		 * @arg name name of the graph being dumped
		 * @arg directed will the dumping graph be directed
		 *
		 * This function begins graph's dump named 'name' into file with
		 * given path.
		 *
		 * @code
		 * DotViz::DVGraph graph;
		 *
		 * graph.begin("out.gv", "test_graph");
		 * graph.end();
		 * @endcode
		 *
		 * @see end(), node(), link(), nodeStyle(), linkStyle().
		 */
		//}================================================================================================================	
		void begin(::std::string path, ::std::string name, bool directed = true);

		//{================================================================================================================
		/**
		 * Create node of graph
		 *
		 * @arg id unique identifier of the node
		 * @arg label string going to be printed in created node.
		 *
		 * This function dumps into graph's file node with current node
		 * drawing style.
		 *
		 * @code
		 * DotViz::DVGraph graph;
		 * 
		 * graph.begin("out.gv", "test_graph");
		 * graph.node(0, "Test node");
		 * graph.end();
		 * @endcode
		 *
		 * @see begin(), end(), link(), nodeStyle(), linkStyle().
		 */
		//}================================================================================================================
		void node(long long unsigned id, ::std::string label);

		//{================================================================================================================
		/**
		 * Current node drawing style
		 *
		 * This function returns you reference to current nodes' drawing
		 * style in the graph.
		 *
		 * @code
		 * DotViz::DVGraph graph;
		 * ...
		 * graph.nodeStyle().color("red");
		 * @endcode
		 *
		 * @return Reference to graph's nodes drawing style
		 *
		 * @see begin(), end(), node(), nodeStyle(), link(),
		 * linkStyle().
		 */
		//}================================================================================================================	
		DVNodeStyle &nodeStyle();
	
		//{================================================================================================================
		/**
		 * Substitute node drawing style
		 *
		 * @arg src new graph's nodes drawing style
		 *
		 * This function substitutes current nodes' drawing style in the
		 * graph, with the given one.
		 *
		 * @code
		 * DotViz::DVGraph graph;
		 * DotViz::DVNodeStyle ns;
		 * ....
		 * grap.nodeStyle(ns);
		 * @endcode
		 *
		 * @return Reference to graph's nodes drawing style
		 *
		 * @see begin(), end(), node(), nodeStyle(), link(),
		 * linkStyle().
		 */
		//}================================================================================================================	
		DVNodeStyle &nodeStyle(const DVNodeStyle &src);

		//{================================================================================================================
		/**
		 * Create link of graph
		 *
		 * @arg id0 first node of link, where link comes from
		 * @arg id1 second node of link, where link comes to
		 * 
		 * This function connects two nodes of graph id0 and id1 with a
		 * link and draw link as directed, if in begin the graph was
		 * initialised as directed.
		 *
		 * @code
		 * DotViz::DVGraph graph;
		 *
		 * graph.begin("out.gv", "test");
		 * graph.node(0, "Europe");
		 * graph.node(1, "Asia");
		 * graph.node(2, "Eurasia");
		 * graph.link(2, 0);
		 * graph.link(2, 1);
		 * graph.end();
		 * @endcode
		 *
		 * @see begin(), end(), node(), nodeStyle(), linkStyle().
		 */
		//}================================================================================================================	
		void link(long long unsigned id0, long long unsigned id1);

		//{================================================================================================================
		/**
		 * Current link drawing style
		 *
		 * This function returns you reference to current links' drawing
		 * style in the graph.
		 *
		 * @code
		 * DotViz::DVGraph graph;
		 * ...
		 * graph.linkStyle().color("#FFDEAD");
		 * @endcode
		 *
		 * @return Reference to the graph's links drawing style
		 *
		 * @see begin(), end(), node(), nodeStyle(), link(),
		 * linkStyle().
		 */
		//}================================================================================================================	
		inline DVLinkStyle &linkStyle();

		//{================================================================================================================
		/**
		 * Substitute link drawing style
		 *
		 * @arg src new graph's links drawing style
		 *
		 * This function substitutes current links' drawing style in the
		 * graph, with the given one.
		 *
		 * @code
		 * DotViz::DVGraph graph;
		 * DotViz::DVNodeStyle ns;
		 * ....
		 * grap.nodeStyle(ns);
		 * @endcode
		 *
		 * @return Reference to link style of the graph, after the substitution
		 *
		 * @see begin(), end(), node(), nodeStyle(), link(),
		 * linkStyle().
		 */
		//}================================================================================================================
		inline DVLinkStyle &linkStyle(const DVLinkStyle &src);

		//{================================================================================================================
		/**
		 * End graph drawing
		 *
		 * This function end the graph drawing, closing file, 
		 * where dumping of graph is being proceeded.
		 *
		 * @code
		 * DotViz::DVGraph graph;
		 *
		 * graph.begin("out.gv", "test_graph");
		 * graph.end();
		 * @endcode
		 *
		 * @see begin(), end(), node(), nodeStyle(), link(),
		 * linkStyle().
		 */
		//}================================================================================================================	
	
		void end();
	};

	//{===================================================================================================================
	/**
	 * @ingroup stuff
	 * Render pre-descripted graph to image
	 *
	 * @arg in input graph definition done by DotViz or manually in dot language
	 * @arg out output file path, extension will be cut automatically from it
	 *
	 * This function renderes graph by its description into image.
	 *
	 * @code
	 * ...
	 * DotViz::dvRender("out.gv", "out.png");
	 * @endcode
	 *
	 * @see DVGraph.
	 */
	//}===================================================================================================================
	void dvRender(::std::string in, ::std::string out);
}

#endif // DOTVIZ_PLUS_PLUS_HPP
