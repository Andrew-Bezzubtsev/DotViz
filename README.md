# DotViz
DotViz is a small cross-platform port for tree visualisation.
It generates Dot-language description of the tree as a graph and can render it if you want.
It is debugged for Windows, OS X and Linux. Report about bygs into issues, please.
