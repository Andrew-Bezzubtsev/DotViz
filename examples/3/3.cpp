#include <DotViz++.hpp>
#include <stdlib.h>
#include <assert.h>
#include <string>

struct bidirected_list {
	bidirected_list *next;
	bidirected_list *prev;
	int val;
};

bidirected_list *bidirected_list_init(int val);
void bidirected_list_insert_before(bidirected_list *node,
				   int val);
void bidirected_list_insert_after(bidirected_list *node,
				  int val);
void bidirected_list_remove_before(bidirected_list *node);
void bidirected_list_remove_after(bidirected_list *node);
void bidirected_list_dump(std::string path, std::string name,
			  bidirected_list *node);
void bidirected_list_delete(bidirected_list *node);

int main()
{
	bidirected_list *list = bidirected_list_init(rand() % 500);

	for (int i = 0; i < 3; i++) {
		bidirected_list_insert_before(list, rand() % 500);
		bidirected_list_insert_after(list, rand() % 500);
	}

	bidirected_list_dump("out.gv", "bidirected_list_dump", list);
	DotViz::dvRender("out.gv", "out.png");
	bidirected_list_delete(list);

	return 0;
}

bidirected_list *bidirected_list_init(int val)
{
	bidirected_list *res = (bidirected_list *)calloc(1, sizeof(*res));
	assert(res);

	res->val = val;
	return res;
}

void bidirected_list_insert_before(bidirected_list *node, 
				   int val)
{
	assert(node);

	bidirected_list *res = bidirected_list_init(val);
	res->next = node;
	res->prev = node->prev;
	node->prev = res;
}

void bidirected_list_insert_after(bidirected_list *node,
				  int val)
{
	assert(node);

	bidirected_list *res = bidirected_list_init(val);
	res->prev = node;
	res->next = node->next;
	node->next = res;
}

void bidirected_list_remove_before(bidirected_list *node)
{
	assert(node);

	if (!(node->prev))
		return;

	bidirected_list *prev = node->prev->prev;
	free((void *)node->prev);
	node->prev = prev;
	node->prev->next = node;
}

void bidirected_list_remove_after(bidirected_list *node)
{
	assert(node);

	if (!(node->next))
		return;

	bidirected_list *next = node->next->next;
	free((void *)node->next);
	node->next = next;
	node->next->prev = node;
}

void bidirected_list_dump(std::string path, std::string name,
			  bidirected_list *node)
{
	DotViz::DVGraph graph;
	graph.begin(path, name);

	if (node) {
		size_t i = 0, j = 0;
		bidirected_list *root = node;

		while (node) {
			graph.node(i, std::to_string(node->val));
			if (i) {
				graph.link(i, i - 1);
				graph.link(i - 1, i);
			}

			node = node->prev;
			i++;
		}

		node = root->next;
		while (node) {
			graph.node(i + j, std::to_string(node->val));
			if (j) {
				graph.link(i + j, i + j - 1);
				graph.link(i + j - 1, i + j);
			}

			node = node->next;
			j++;
		}

		graph.link(0, i);
		graph.link(i, 0);
	}

	graph.end();
}

void bidirected_list_delete(bidirected_list *node)
{
	bidirected_list *cp = node->next;
	while (node) {
		bidirected_list *prev = node->prev;
		free((void *)node);
		node = prev;
	}

	node = cp;
	while (node) {
		bidirected_list *next = node->next;
		free((void *)node);
		node = next;
	}
}
