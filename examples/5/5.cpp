#include <DotViz++.hpp>
#include <assert.h>
#include <stdlib.h>
#include <vector>
#include <string>

struct node {
	size_t id;
	std::string val;
};

struct edge {
	size_t id0;
	size_t id1;
};

struct graph {
	std::vector<node> nodes;
	std::vector<edge> edges;
};

node node_init(size_t id, std::string val);
edge edge_init(size_t id0, size_t id1);
graph *graph_init();
void graph_add_node(graph *gr, node val);
void graph_add_edge(graph *gr, edge val);
void graph_dump(std::string path, std::string name,
		graph *gr);
void graph_delete(graph *gr);

int main()
{
	graph *gr = graph_init();

	graph_add_node(gr, node_init(0, "Moscow"));
	graph_add_node(gr, node_init(1, "Anapa"));
	graph_add_node(gr, node_init(2, "Sochi"));
	graph_add_node(gr, node_init(3, "Adler"));
	graph_add_node(gr, node_init(4, "Abrau Durso"));

	graph_add_edge(gr, edge_init(0, 2));
	graph_add_edge(gr, edge_init(2, 3));

	graph_add_edge(gr, edge_init(2, 1));
	graph_add_edge(gr, edge_init(1, 0));
	graph_add_edge(gr, edge_init(0, 4));

	graph_dump("out.gv", "test_graph_dump", gr);
	DotViz::dvRender("out.gv", "out.png");

	graph_delete(gr);

	return 0;
}

node node_init(size_t id, std::string val)
{
	return node{id, val};
}

edge edge_init(size_t id0, size_t id1)
{
	return edge{id0, id1};
}

graph *graph_init()
{
	graph *res = (graph *)calloc(1, sizeof(*res));
	assert(res);

	return res;
}

void graph_add_node(graph *gr, node val)
{
	assert(gr);
	gr->nodes.push_back(val);
}

void graph_add_edge(graph *gr, edge val)
{
	assert(gr);
	gr->edges.push_back(val);
}

void graph_dump(std::string path, std::string name, 
		graph *gr)
{
	DotViz::DVGraph out;
	out.begin(path, name);

	if (!gr)
		return;

	for (size_t i = 0; i < gr->nodes.size(); i++)
		out.node(gr->nodes[i].id, gr->nodes[i].val);

	for (size_t i = 0; i < gr->edges.size(); i++)
		out.link(gr->edges[i].id0, gr->edges[i].id1);

	out.end();
}

void graph_delete(graph *gr)
{
	if (!gr)
		return;

	gr->nodes.clear();
	gr->edges.clear();
	free((void *)gr);
}
