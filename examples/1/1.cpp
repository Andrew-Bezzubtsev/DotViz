#include <DotViz++.hpp>

int main()
{
	DotViz::DVGraph graph;
	graph.begin("out.gv", "test1");

	graph.node(0, "Distribution");
	graph.node(1, "Heaven");
	graph.node(2, "Hell");
	graph.link(0, 1);
	graph.link(0, 2);

	graph.end();

	DotViz::dvRender("out.gv", "out.png");

	return 0;
}
