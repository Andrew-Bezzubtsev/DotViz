#include <DotViz++.hpp>
#include <stdlib.h>
#include <assert.h>
#include <string>

struct sort_tree {
	sort_tree *left;
	sort_tree *right;
	int val;
};

sort_tree *sort_tree_init(int val);
sort_tree *sort_tree_insert(sort_tree *tree, int val);
void sort_tree_dump(std::string path, std::string name,
		    sort_tree *tree);
void sort_tree_delete(sort_tree *tree);

int main()
{
	sort_tree *tree = 0;

	for (int i = 0; i < 10; i++)
		tree = sort_tree_insert(tree, rand() % 1000);

	sort_tree_insert(tree, 5);

	sort_tree_dump("out.gv", "sort_tree_dump", tree);
	DotViz::dvRender("out.gv", "out.png");

	sort_tree_delete(tree);

	return 0;
}

sort_tree *sort_tree_init(int val)
{
	sort_tree *res = (sort_tree *)calloc(1, sizeof(*res));
	assert(res);

	res->val = val;

	return res;
}

sort_tree *sort_tree_insert(sort_tree *tree, int val)
{
	sort_tree *root = tree;
	sort_tree *parent = 0;

	while (tree && val != tree->val) {
		if (val < tree->val) {
			parent = tree;
			tree = tree->left;
		} else {
			parent = tree;
			tree = tree->right;
		}
	}

	if (!tree && parent) {
		if (val > parent->val)
			parent->right = sort_tree_init(val);
		else
			parent->left = sort_tree_init(val);

		return root;
	}

	return sort_tree_init(val);
}

static void __dump_node(DotViz::DVGraph &graph, sort_tree *tree)
{
	if (!tree)
		return;

	graph.node(tree->val, std::to_string(tree->val));
	if (tree->left) {
		__dump_node(graph, tree->left);
		graph.link(tree->val, tree->left->val);
	}

	if (tree->right) {
		__dump_node(graph, tree->right);
		graph.link(tree->val, tree->right->val);
	}	
}

void sort_tree_dump(std::string path, std::string name,
		    sort_tree *tree)
{
	DotViz::DVGraph graph;
	graph.begin(path, name);

	__dump_node(graph, tree);

	graph.end();
}

void sort_tree_delete(sort_tree *tree)
{
	if (!tree)
		return;

	sort_tree_delete(tree->left);
	sort_tree_delete(tree->right);
	free((void *)tree);
}
