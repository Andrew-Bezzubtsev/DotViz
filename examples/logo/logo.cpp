#include "../../DotViz++.hpp"

int main()
{
	DotViz::DVGraph logo;
	logo.begin("logo.gv", "logoGraph");

	logo.node(0, "DotViz");
	logo.node(1, "Dot");
	logo.node(2, "Visualizer");

        logo.link(0, 1);
        logo.link(0, 2);

	logo.end();

	DotViz::dvRender("logo.gv", "logo.png");

	return 0;
}
