#include <DotViz++.hpp>
#include <stdlib.h>
#include <assert.h>
#include <string>

struct unidirected_list {
	unidirected_list *next;
	int val;
};

unidirected_list *unidirected_list_init(int val);
unidirected_list *unidirected_list_insert_head(unidirected_list *old_head,
					       int val);
void unidirected_list_dump(std::string path, std::string name,
			   unidirected_list *head);
void unidirected_list_delete(unidirected_list *head);

int main()
{
	unidirected_list *list = 0;

	for (int i = 0; i < 5; i++)
		list = unidirected_list_insert_head(list, rand() % 500);

	unidirected_list_dump("out.gv", "ListDump", list);
	unidirected_list_delete(list);

	DotViz::dvRender("out.gv", "out.png");

	return 0;
}

unidirected_list *unidirected_list_init(int val)
{
	unidirected_list *res = (unidirected_list *)calloc(1, sizeof(*res));
	assert(res);

	res->val = val;
	return res;
}

unidirected_list *unidirected_list_insert_head(unidirected_list *old_head,
					       int val)
{
	unidirected_list *new_head = unidirected_list_init(val);
	assert(new_head);

	new_head->next = old_head;
	return new_head;
}

void unidirected_list_dump(std::string path, std::string name,
			   unidirected_list *head)
{
	DotViz::DVGraph graph;
	graph.begin(path, name);

	size_t i = 0;
	while (head) {
		graph.node(i, std::to_string(head->val));

		if (i)
			graph.link(i - 1, i);

		head = head->next;
		i++;
	}

	graph.end();
}

void unidirected_list_delete(unidirected_list *head)
{
	while (head) {
		unidirected_list *next = head->next;
		free((void *)head);
		head = next;
	}
}
